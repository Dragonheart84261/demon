// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DemonSlayerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DEMONSLAYER_API ADemonSlayerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
